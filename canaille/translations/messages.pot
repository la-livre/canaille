# Translations template for PROJECT.
# Copyright (C) 2023 ORGANIZATION
# This file is distributed under the same license as the PROJECT project.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2023.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2023-03-29 23:00+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.12.1\n"

#: canaille/account.py:105 canaille/account.py:130
#: canaille/oidc/endpoints.py:84
msgid "Login failed, please check your information"
msgstr ""

#: canaille/account.py:136 canaille/account.py:621
#, python-format
msgid "Connection successful. Welcome %(user)s"
msgstr ""

#: canaille/account.py:149
#, python-format
msgid "You have been disconnected. See you next time %(user)s"
msgstr ""

#: canaille/account.py:169
msgid ""
"A password initialization link has been sent at your email address. You "
"should receive it within a few minutes."
msgstr ""

#: canaille/account.py:175 canaille/account.py:524
msgid "Could not send the password initialization email"
msgstr ""

#: canaille/account.py:267 canaille/account.py:295
msgid "The invitation link that brought you here was invalid."
msgstr ""

#: canaille/account.py:274
msgid "The invitation link that brought you here has expired."
msgstr ""

#: canaille/account.py:281
msgid "Your account has already been created."
msgstr ""

#: canaille/account.py:288
msgid "You are already logged in, you cannot create an account."
msgstr ""

#: canaille/account.py:307 canaille/forms.py:289 canaille/forms.py:370
#: canaille/templates/groups.html:5 canaille/templates/groups.html:25
#: canaille/templates/partial/users.html:18
#: canaille/themes/default/base.html:61
msgid "Groups"
msgstr ""

#: canaille/account.py:329 canaille/account.py:358
msgid "User account creation failed."
msgstr ""

#: canaille/account.py:334
msgid "Your account has been created successfuly."
msgstr ""

#: canaille/account.py:399
msgid "User account creation succeed."
msgstr ""

#: canaille/account.py:460 canaille/account.py:567
msgid "Profile edition failed."
msgstr ""

#: canaille/account.py:486 canaille/account.py:582
msgid "Profile updated successfuly."
msgstr ""

#: canaille/account.py:518
msgid ""
"A password initialization link has been sent at the user email address. "
"It should be received within a few minutes."
msgstr ""

#: canaille/account.py:531
msgid ""
"A password reset link has been sent at the user email address. It should "
"be received within a few minutes."
msgstr ""

#: canaille/account.py:537
msgid "Could not send the password reset email"
msgstr ""

#: canaille/account.py:602
#, python-format
msgid "The user %(user)s has been sucessfuly deleted"
msgstr ""

#: canaille/account.py:636
msgid "Could not send the password reset link."
msgstr ""

#: canaille/account.py:640
msgid ""
"A password reset link has been sent at your email address. You should "
"receive it within a few minutes."
msgstr ""

#: canaille/account.py:651
#, python-format
msgid ""
"The user '%(user)s' does not have permissions to update their password. "
"We cannot send a password reset email."
msgstr ""

#: canaille/account.py:666
msgid "We encountered an issue while we sent the password recovery email."
msgstr ""

#: canaille/account.py:685
msgid "The password reset link that brought you here was invalid."
msgstr ""

#: canaille/account.py:694
msgid "Your password has been updated successfuly"
msgstr ""

#: canaille/admin.py:23 canaille/templates/partial/users.html:15
msgid "Email"
msgstr ""

#: canaille/admin.py:29 canaille/forms.py:85 canaille/forms.py:109
#: canaille/forms.py:197 canaille/forms.py:364
msgid "jane@doe.com"
msgstr ""

#: canaille/admin.py:42
msgid "The test invitation mail has been sent correctly"
msgstr ""

#: canaille/admin.py:44
msgid "The test invitation mail has not been sent correctly"
msgstr ""

#: canaille/admin.py:89 canaille/mails.py:187
msgid "Password initialization on {website_name}"
msgstr ""

#: canaille/admin.py:131 canaille/mails.py:147
msgid "Password reset on {website_name}"
msgstr ""

#: canaille/admin.py:173
msgid "Invitation on {website_name}"
msgstr ""

#: canaille/apputils.py:32
msgid "John Doe"
msgstr ""

#: canaille/apputils.py:35 canaille/forms.py:153 canaille/forms.py:352
msgid "jdoe"
msgstr ""

#: canaille/apputils.py:38
msgid "john@doe.com"
msgstr ""

#: canaille/apputils.py:40
msgid " or "
msgstr ""

#: canaille/flaskutils.py:63
msgid "No SMTP server has been configured"
msgstr ""

#: canaille/forms.py:19
msgid "This is not a valid URL"
msgstr ""

#: canaille/forms.py:27
msgid "The login '{login}' already exists"
msgstr ""

#: canaille/forms.py:36
msgid "The email '{email}' is already used"
msgstr ""

#: canaille/forms.py:43
msgid "The group '{group}' already exists"
msgstr ""

#: canaille/forms.py:52
msgid "The login '{login}' does not exist"
msgstr ""

#: canaille/forms.py:77
msgid "The page number is not valid"
msgstr ""

#: canaille/forms.py:82 canaille/forms.py:106
#: canaille/templates/partial/users.html:9
msgid "Login"
msgstr ""

#: canaille/forms.py:95 canaille/forms.py:118 canaille/forms.py:242
msgid "Password"
msgstr ""

#: canaille/forms.py:125 canaille/forms.py:249
msgid "Password confirmation"
msgstr ""

#: canaille/forms.py:128 canaille/forms.py:252
msgid "Password and confirmation do not match."
msgstr ""

#: canaille/forms.py:147
msgid "Automatic"
msgstr ""

#: canaille/forms.py:152 canaille/forms.py:351
msgid "Username"
msgstr ""

#: canaille/forms.py:156 canaille/forms.py:323 canaille/forms.py:337
#: canaille/oidc/forms.py:22 canaille/templates/partial/groups.html:6
#: canaille/templates/partial/oidc/admin/client_list.html:6
#: canaille/templates/partial/users.html:12
msgid "Name"
msgstr ""

#: canaille/forms.py:158
msgid "Title"
msgstr ""

#: canaille/forms.py:158
msgid "Vice president"
msgstr ""

#: canaille/forms.py:161
msgid "Given name"
msgstr ""

#: canaille/forms.py:163
msgid "John"
msgstr ""

#: canaille/forms.py:169
msgid "Family Name"
msgstr ""

#: canaille/forms.py:172
msgid "Doe"
msgstr ""

#: canaille/forms.py:178
msgid "Display Name"
msgstr ""

#: canaille/forms.py:181
msgid "Johnny"
msgstr ""

#: canaille/forms.py:187 canaille/forms.py:357
msgid "Email address"
msgstr ""

#: canaille/forms.py:193
msgid ""
"This email will be used as a recovery address to reset the password if "
"needed"
msgstr ""

#: canaille/forms.py:203
msgid "Phone number"
msgstr ""

#: canaille/forms.py:203
msgid "555-000-555"
msgstr ""

#: canaille/forms.py:206
msgid "Address"
msgstr ""

#: canaille/forms.py:208
msgid "132, Foobar Street, Gotham City 12401, XX"
msgstr ""

#: canaille/forms.py:212
msgid "Street"
msgstr ""

#: canaille/forms.py:214
msgid "132, Foobar Street"
msgstr ""

#: canaille/forms.py:218
msgid "Postal Code"
msgstr ""

#: canaille/forms.py:224
msgid "Locality"
msgstr ""

#: canaille/forms.py:226
msgid "Gotham City"
msgstr ""

#: canaille/forms.py:230
msgid "Region"
msgstr ""

#: canaille/forms.py:232
msgid "North Pole"
msgstr ""

#: canaille/forms.py:236
msgid "Photo"
msgstr ""

#: canaille/forms.py:240 canaille/templates/profile_add.html:63
#: canaille/templates/profile_edit.html:81
msgid "Delete the photo"
msgstr ""

#: canaille/forms.py:260
msgid "User number"
msgstr ""

#: canaille/forms.py:262 canaille/forms.py:268
msgid "1234"
msgstr ""

#: canaille/forms.py:266
msgid "Department number"
msgstr ""

#: canaille/forms.py:272
msgid "Organization"
msgstr ""

#: canaille/forms.py:274
msgid "Cogip LTD."
msgstr ""

#: canaille/forms.py:278
msgid "Website"
msgstr ""

#: canaille/forms.py:280
msgid "https://mywebsite.tld"
msgstr ""

#: canaille/forms.py:285
msgid "Preferred language"
msgstr ""

#: canaille/forms.py:291
msgid "users, admins …"
msgstr ""

#: canaille/forms.py:326
msgid "group"
msgstr ""

#: canaille/forms.py:330 canaille/forms.py:344
#: canaille/templates/partial/groups.html:7
msgid "Description"
msgstr ""

#: canaille/forms.py:355
msgid "Username editable by the invitee"
msgstr ""

#: canaille/groups.py:38
msgid "Group creation failed."
msgstr ""

#: canaille/groups.py:46
#, python-format
msgid "The group %(group)s has been sucessfully created"
msgstr ""

#: canaille/groups.py:98
#, python-format
msgid "The group %(group)s has been sucessfully edited."
msgstr ""

#: canaille/groups.py:106
msgid "Group edition failed."
msgstr ""

#: canaille/groups.py:120
#, python-format
msgid "The group %(group)s has been sucessfully deleted"
msgstr ""

#: canaille/mails.py:109
msgid "Test email from {website_name}"
msgstr ""

#: canaille/mails.py:217
msgid "You have been invited to create an account on {website_name}"
msgstr ""

#: canaille/ldap_backend/backend.py:61
msgid "Could not connect to the LDAP server '{uri}'"
msgstr ""

#: canaille/ldap_backend/backend.py:77
msgid "LDAP authentication failed with user '{user}'"
msgstr ""

#: canaille/oidc/clients.py:46
msgid "The client has not been added. Please check your information."
msgstr ""

#: canaille/oidc/clients.py:82
msgid "The client has been created."
msgstr ""

#: canaille/oidc/clients.py:126
msgid "The client has not been edited. Please check your information."
msgstr ""

#: canaille/oidc/clients.py:155
msgid "The client has been edited."
msgstr ""

#: canaille/oidc/clients.py:168
msgid "The client has been deleted."
msgstr ""

#: canaille/oidc/consents.py:73 canaille/oidc/consents.py:112
msgid "Could not revoke this access"
msgstr ""

#: canaille/oidc/consents.py:76
msgid "The access is already revoked"
msgstr ""

#: canaille/oidc/consents.py:80 canaille/oidc/consents.py:129
msgid "The access has been revoked"
msgstr ""

#: canaille/oidc/consents.py:91
msgid "Could not restore this access"
msgstr ""

#: canaille/oidc/consents.py:94
msgid "The access is not revoked"
msgstr ""

#: canaille/oidc/consents.py:101
msgid "The access has been restored"
msgstr ""

#: canaille/oidc/endpoints.py:135
msgid "You have been successfully logged out."
msgstr ""

#: canaille/oidc/endpoints.py:341
msgid "You have been disconnected"
msgstr ""

#: canaille/oidc/endpoints.py:358
msgid "You have not been disconnected"
msgstr ""

#: canaille/oidc/forms.py:27
msgid "Contact"
msgstr ""

#: canaille/oidc/forms.py:32
msgid "URI"
msgstr ""

#: canaille/oidc/forms.py:40
msgid "Redirect URIs"
msgstr ""

#: canaille/oidc/forms.py:48
msgid "Post logout redirect URIs"
msgstr ""

#: canaille/oidc/forms.py:56
msgid "Grant types"
msgstr ""

#: canaille/oidc/forms.py:68 canaille/templates/oidc/admin/token_view.html:79
msgid "Scope"
msgstr ""

#: canaille/oidc/forms.py:74
msgid "Response types"
msgstr ""

#: canaille/oidc/forms.py:80
msgid "Token Endpoint Auth Method"
msgstr ""

#: canaille/oidc/forms.py:90
msgid "Token audiences"
msgstr ""

#: canaille/oidc/forms.py:96
msgid "Logo URI"
msgstr ""

#: canaille/oidc/forms.py:104
msgid "Terms of service URI"
msgstr ""

#: canaille/oidc/forms.py:112
msgid "Policy URI"
msgstr ""

#: canaille/oidc/forms.py:120
msgid "Software ID"
msgstr ""

#: canaille/oidc/forms.py:125
msgid "Software Version"
msgstr ""

#: canaille/oidc/forms.py:130
msgid "JWK"
msgstr ""

#: canaille/oidc/forms.py:135
msgid "JKW URI"
msgstr ""

#: canaille/oidc/forms.py:143
msgid "Pre-consent"
msgstr ""

#: canaille/oidc/tokens.py:58
msgid "The token has successfully been revoked."
msgstr ""

#: canaille/oidc/utils.py:6
msgid "Info about yourself, such as your name."
msgstr ""

#: canaille/oidc/utils.py:8
msgid "Your e-mail address."
msgstr ""

#: canaille/oidc/utils.py:9
msgid "Your postal address."
msgstr ""

#: canaille/oidc/utils.py:10
msgid "Your phone number."
msgstr ""

#: canaille/oidc/utils.py:11
msgid "Groups you belong to."
msgstr ""

#: canaille/templates/about.html:5 canaille/templates/about.html:16
#: canaille/themes/default/base.html:91
msgid "About Canaille"
msgstr ""

#: canaille/templates/about.html:18
msgid "Free and open-source identity provider."
msgstr ""

#: canaille/templates/about.html:21
#, python-format
msgid "Version %(version)s"
msgstr ""

#: canaille/templates/about.html:22
msgid "Homepage"
msgstr ""

#: canaille/templates/about.html:23
msgid "Documentation"
msgstr ""

#: canaille/templates/about.html:24
msgid "Source code"
msgstr ""

#: canaille/templates/error.html:19
msgid "Bad request"
msgstr ""

#: canaille/templates/error.html:21
msgid "Unauthorized"
msgstr ""

#: canaille/templates/error.html:23
msgid "Page not found"
msgstr ""

#: canaille/templates/error.html:25
msgid "Technical problem"
msgstr ""

#: canaille/templates/error.html:31
msgid "The request you made is invalid"
msgstr ""

#: canaille/templates/error.html:33
msgid "You do not have the authorizations to access this page"
msgstr ""

#: canaille/templates/error.html:35
msgid "The page you are looking for does not exist"
msgstr ""

#: canaille/templates/error.html:37
msgid "Please contact your administrator"
msgstr ""

#: canaille/templates/firstlogin.html:11
msgid "First login"
msgstr ""

#: canaille/templates/firstlogin.html:16
msgid ""
"It seems this is the first time you are logging here. In order to "
"finalize your account configuration, you need to set a password to your "
"account. We will send you an email containing a link that will allow you "
"to set a password. Please click on the \"Send the initialization email\" "
"button below to send the email."
msgstr ""

#: canaille/templates/firstlogin.html:28
#: canaille/templates/forgotten-password.html:32
msgid "Login page"
msgstr ""

#: canaille/templates/firstlogin.html:29
msgid "Send the initialization email"
msgstr ""

#: canaille/templates/forgotten-password.html:5
#: canaille/templates/forgotten-password.html:15
#: canaille/templates/login.html:36 canaille/templates/password.html:33
msgid "Forgotten password"
msgstr ""

#: canaille/templates/forgotten-password.html:20
msgid ""
"After this form is sent, if the email address or the login you provided "
"exists, you will receive an email containing a link that will allow you "
"to reset your password."
msgstr ""

#: canaille/templates/forgotten-password.html:35
msgid "Send again"
msgstr ""

#: canaille/templates/forgotten-password.html:37
#: canaille/templates/mail/admin.html:52
msgid "Send"
msgstr ""

#: canaille/templates/group.html:7 canaille/templates/group.html:54
msgid "Group creation"
msgstr ""

#: canaille/templates/group.html:9 canaille/templates/group.html:56
msgid "Group edition"
msgstr ""

#: canaille/templates/group.html:22 canaille/templates/groups.html:12
#: canaille/templates/invite.html:16 canaille/templates/profile_add.html:17
#: canaille/templates/users.html:16
msgid "View"
msgstr ""

#: canaille/templates/group.html:26 canaille/templates/groups.html:16
#: canaille/templates/invite.html:20 canaille/templates/profile_add.html:21
#: canaille/templates/users.html:20
msgid "Add"
msgstr ""

#: canaille/templates/group.html:36
msgid "Group deletion"
msgstr ""

#: canaille/templates/group.html:40
msgid ""
"Are you sure you want to delete this group? This action is unrevokable "
"and all the data about this group will be removed."
msgstr ""

#: canaille/templates/group.html:44
#: canaille/templates/oidc/admin/client_edit.html:47
#: canaille/templates/oidc/admin/token_view.html:50
#: canaille/templates/profile_settings.html:60
msgid "Cancel"
msgstr ""

#: canaille/templates/group.html:45
#: canaille/templates/oidc/admin/client_edit.html:48
#: canaille/templates/oidc/admin/token_view.html:51
#: canaille/templates/profile_settings.html:61
msgid "Delete"
msgstr ""

#: canaille/templates/group.html:62
msgid "Create a new group"
msgstr ""

#: canaille/templates/group.html:64
msgid "Edit information about a group"
msgstr ""

#: canaille/templates/group.html:75
msgid ""
"Because group cannot be empty, you will be added to the group. You can "
"remove you later by editing your profile when you will have added other "
"members to the group."
msgstr ""

#: canaille/templates/group.html:83
msgid "Delete group"
msgstr ""

#: canaille/templates/group.html:88
msgid "Create group"
msgstr ""

#: canaille/templates/group.html:90 canaille/templates/profile_add.html:210
#: canaille/templates/profile_edit.html:181
#: canaille/templates/profile_settings.html:151
msgid "Submit"
msgstr ""

#: canaille/templates/group.html:102
msgid "Group members"
msgstr ""

#: canaille/templates/invite.html:5 canaille/templates/invite.html:99
msgid "Invite a user"
msgstr ""

#: canaille/templates/invite.html:24 canaille/templates/profile_add.html:26
#: canaille/templates/users.html:25
msgid "Invite"
msgstr ""

#: canaille/templates/invite.html:37
msgid "Invitation link"
msgstr ""

#: canaille/templates/invite.html:39
msgid "Invitation sent"
msgstr ""

#: canaille/templates/invite.html:41
msgid "Invitation not sent"
msgstr ""

#: canaille/templates/invite.html:49
msgid ""
"Here is the invitation link you can provide to the user you want to "
"invite:"
msgstr ""

#: canaille/templates/invite.html:56
#, python-format
msgid "This invitation link has been sent to %(email)s"
msgstr ""

#: canaille/templates/invite.html:57
msgid ""
"If you need to provide this link by other ways than email, you can copy "
"it there:"
msgstr ""

#: canaille/templates/invite.html:64
#, python-format
msgid ""
"This invitation link could not be sent to %(email)s due to technical "
"issues."
msgstr ""

#: canaille/templates/invite.html:65
msgid ""
"However you can copy the link there to provide it by other ways than "
"email:"
msgstr ""

#: canaille/templates/invite.html:76
#: canaille/templates/oidc/admin/token_view.html:135
#: canaille/templates/oidc/admin/token_view.html:146
msgid "Copy"
msgstr ""

#: canaille/templates/invite.html:83 canaille/templates/invite.html:130
msgid "Create a user"
msgstr ""

#: canaille/templates/invite.html:87
msgid "Invite another user"
msgstr ""

#: canaille/templates/invite.html:104
msgid ""
"After this form is sent, the recipient your indicated will receive an "
"email containing an account creation link."
msgstr ""

#: canaille/templates/invite.html:133
msgid "Generate a link"
msgstr ""

#: canaille/templates/invite.html:136
msgid "Send the invitation"
msgstr ""

#: canaille/templates/login.html:19
#, python-format
msgid "Sign in at %(website)s"
msgstr ""

#: canaille/templates/login.html:21
msgid "Manage your information and your authorizations"
msgstr ""

#: canaille/templates/login.html:38
msgid "Continue"
msgstr ""

#: canaille/templates/password.html:19
#, python-format
msgid "Sign in as %(username)s"
msgstr ""

#: canaille/templates/password.html:21
msgid "Please enter your password for this account."
msgstr ""

#: canaille/templates/password.html:31
#, python-format
msgid "I am not %(username)s"
msgstr ""

#: canaille/templates/password.html:35
msgid "Sign in"
msgstr ""

#: canaille/templates/profile_add.html:5 canaille/templates/profile_add.html:40
#: canaille/templates/profile_edit.html:6
msgid "User creation"
msgstr ""

#: canaille/templates/profile_add.html:44
msgid "Create a new user account"
msgstr ""

#: canaille/templates/profile_add.html:49
#: canaille/templates/profile_edit.html:23
#: canaille/templates/profile_settings.html:21
msgid "Personal information"
msgstr ""

#: canaille/templates/profile_add.html:60
#: canaille/templates/profile_add.html:71
#: canaille/templates/profile_edit.html:78
#: canaille/templates/profile_edit.html:89
msgid "Click to upload a photo"
msgstr ""

#: canaille/templates/profile_add.html:161
#: canaille/templates/profile_edit.html:27
#: canaille/templates/profile_settings.html:25
#: canaille/templates/profile_settings.html:69
msgid "Account information"
msgstr ""

#: canaille/templates/profile_add.html:185
msgid "User password is not mandatory"
msgstr ""

#: canaille/templates/profile_add.html:188
msgid "The user password can be set:"
msgstr ""

#: canaille/templates/profile_add.html:190
msgid "by filling this form;"
msgstr ""

#: canaille/templates/profile_add.html:191
msgid ""
"by sending the user a password initialization mail, after the account "
"creation;"
msgstr ""

#: canaille/templates/profile_add.html:192
#: canaille/templates/profile_settings.html:109
msgid ""
"or simply waiting for the user to sign-in a first time, and then receive "
"a password initialization mail."
msgstr ""

#: canaille/templates/profile_add.html:197
msgid ""
"The user will not be able to authenticate unless the password is set, but"
" they will be able to ask for a password initialization mail."
msgstr ""

#: canaille/templates/profile_add.html:199
msgid "The user will not be able to authenticate unless the password is set."
msgstr ""

#: canaille/templates/profile_edit.html:8
#: canaille/templates/profile_edit.html:52
#: canaille/templates/profile_settings.html:6
msgid "My profile"
msgstr ""

#: canaille/templates/profile_edit.html:10
#: canaille/templates/profile_edit.html:54
#: canaille/templates/profile_settings.html:8
msgid "User profile edition"
msgstr ""

#: canaille/templates/profile_edit.html:41
#: canaille/templates/profile_settings.html:37
msgid "This user cannot edit this field"
msgstr ""

#: canaille/templates/profile_edit.html:43
#: canaille/templates/profile_settings.html:39
msgid "This user cannot see this field"
msgstr ""

#: canaille/templates/profile_edit.html:60
msgid "Edit your personal information"
msgstr ""

#: canaille/templates/profile_edit.html:62
msgid "Edit information about a user"
msgstr ""

#: canaille/templates/profile_settings.html:48
msgid "Account deletion"
msgstr ""

#: canaille/templates/profile_settings.html:53
msgid ""
"Are you sure you want to delete this user? This action is unrevokable and"
" all the data about this user will be removed."
msgstr ""

#: canaille/templates/profile_settings.html:55
msgid ""
"Are you sure you want to delete your account? This action is unrevokable "
"and all your data will be removed forever."
msgstr ""

#: canaille/templates/profile_settings.html:98
msgid "Send email"
msgstr ""

#: canaille/templates/profile_settings.html:102
msgid "This user does not have a password yet"
msgstr ""

#: canaille/templates/profile_settings.html:105
msgid "You can solve this by:"
msgstr ""

#: canaille/templates/profile_settings.html:107
msgid "setting a password using this form;"
msgstr ""

#: canaille/templates/profile_settings.html:108
msgid "sending the user a password initialization mail, by clicking this button;"
msgstr ""

#: canaille/templates/profile_settings.html:112
msgid "The user will not be able to authenticate unless the password is set"
msgstr ""

#: canaille/templates/profile_settings.html:120
msgid "Send mail"
msgstr ""

#: canaille/templates/mail/admin.html:97
#: canaille/templates/profile_settings.html:123
#: canaille/templates/reset-password.html:11
#: canaille/templates/reset-password.html:16
msgid "Password reset"
msgstr ""

#: canaille/templates/profile_settings.html:125
msgid ""
"If the user has forgotten his password, you can send him a password reset"
" email by clicking this button."
msgstr ""

#: canaille/templates/profile_settings.html:137
msgid "Delete the user"
msgstr ""

#: canaille/templates/profile_settings.html:139
msgid "Delete my account"
msgstr ""

#: canaille/templates/profile_settings.html:146
msgid "Impersonate"
msgstr ""

#: canaille/templates/users.html:5 canaille/templates/users.html:35
#: canaille/themes/default/base.html:54
msgid "Users"
msgstr ""

#: canaille/templates/macro/form.html:67
#: canaille/templates/oidc/admin/client_edit.html:63
#: canaille/templates/oidc/admin/client_edit.html:72
#: canaille/templates/oidc/admin/client_edit.html:81
msgid "This field is not editable"
msgstr ""

#: canaille/templates/macro/form.html:71
msgid "This field is required"
msgstr ""

#: canaille/templates/macro/table.html:8
msgid "Search…"
msgstr ""

#: canaille/templates/macro/table.html:20
msgid "Search"
msgstr ""

#: canaille/templates/macro/table.html:65
msgid "Page"
msgstr ""

#: canaille/templates/macro/table.html:111
#, python-format
msgid "%(nb_items)s items"
msgstr ""

#: canaille/templates/mail/admin.html:5 canaille/templates/mail/admin.html:12
#: canaille/templates/oidc/admin/authorization_list.html:16
#: canaille/templates/oidc/admin/authorization_view.html:12
#: canaille/templates/oidc/admin/client_add.html:12
#: canaille/templates/oidc/admin/client_edit.html:16
#: canaille/templates/oidc/admin/client_list.html:16
#: canaille/templates/oidc/admin/token_list.html:16
#: canaille/templates/oidc/admin/token_view.html:17
msgid "Emails"
msgstr ""

#: canaille/templates/mail/admin.html:16
#: canaille/templates/oidc/admin/authorization_list.html:20
#: canaille/templates/oidc/admin/authorization_view.html:16
#: canaille/templates/oidc/admin/client_add.html:16
#: canaille/templates/oidc/admin/client_edit.html:20
#: canaille/templates/oidc/admin/client_list.html:5
#: canaille/templates/oidc/admin/client_list.html:20
#: canaille/templates/oidc/admin/client_list.html:41
#: canaille/templates/oidc/admin/token_list.html:20
#: canaille/templates/oidc/admin/token_view.html:21
msgid "Clients"
msgstr ""

#: canaille/templates/mail/admin.html:20
#: canaille/templates/oidc/admin/authorization_list.html:24
#: canaille/templates/oidc/admin/authorization_view.html:20
#: canaille/templates/oidc/admin/client_add.html:5
#: canaille/templates/oidc/admin/client_add.html:20
#: canaille/templates/oidc/admin/client_add.html:36
#: canaille/templates/oidc/admin/client_edit.html:24
#: canaille/templates/oidc/admin/client_list.html:24
#: canaille/templates/oidc/admin/token_list.html:24
#: canaille/templates/oidc/admin/token_view.html:25
msgid "Add a client"
msgstr ""

#: canaille/templates/mail/admin.html:24
#: canaille/templates/oidc/admin/authorization_list.html:28
#: canaille/templates/oidc/admin/authorization_view.html:24
#: canaille/templates/oidc/admin/client_add.html:24
#: canaille/templates/oidc/admin/client_edit.html:28
#: canaille/templates/oidc/admin/client_list.html:28
#: canaille/templates/oidc/admin/token_list.html:5
#: canaille/templates/oidc/admin/token_list.html:28
#: canaille/templates/oidc/admin/token_list.html:41
#: canaille/templates/oidc/admin/token_view.html:29
msgid "Tokens"
msgstr ""

#: canaille/templates/mail/admin.html:28
#: canaille/templates/oidc/admin/authorization_list.html:5
#: canaille/templates/oidc/admin/authorization_list.html:32
#: canaille/templates/oidc/admin/authorization_list.html:41
#: canaille/templates/oidc/admin/authorization_view.html:28
#: canaille/templates/oidc/admin/client_add.html:28
#: canaille/templates/oidc/admin/client_edit.html:32
#: canaille/templates/oidc/admin/client_list.html:32
#: canaille/templates/oidc/admin/token_list.html:32
#: canaille/templates/oidc/admin/token_view.html:33
msgid "Codes"
msgstr ""

#: canaille/templates/mail/admin.html:37
msgid "Mail sending test"
msgstr ""

#: canaille/templates/mail/admin.html:42
msgid ""
"This form will send a dummy email to the address you want. This should be"
" used for testing mail configuration."
msgstr ""

#: canaille/templates/mail/admin.html:61
msgid "Email preview"
msgstr ""

#: canaille/templates/mail/admin.html:73 canaille/templates/mail/test.html:19
#: canaille/templates/mail/test.txt:1
msgid "Connectivity test"
msgstr ""

#: canaille/templates/mail/admin.html:85
#: canaille/templates/mail/firstlogin.html:19
#: canaille/templates/mail/reset.txt:1
msgid "Password initialization"
msgstr ""

#: canaille/templates/mail/admin.html:109
msgid "Invitation"
msgstr ""

#: canaille/templates/mail/firstlogin.html:27
#, python-format
msgid ""
"In order to finalize your account configuration at %(site_name)s, we need"
" to setup your password. Please click on the \"Initialize password\" "
"button below and follow the instructions."
msgstr ""

#: canaille/templates/mail/firstlogin.html:38
#: canaille/templates/mail/invitation.txt:5 canaille/templates/mail/reset.txt:5
msgid "Initialize password"
msgstr ""

#: canaille/templates/mail/firstlogin.txt:1
#: canaille/templates/mail/reset.html:19
msgid "Password reinitialisation"
msgstr ""

#: canaille/templates/mail/firstlogin.txt:3
#, python-format
msgid ""
"Someone, probably you, asked for a password reinitialization link at "
"%(site_name)s. If you did not asked for this email, please ignore it. If "
"you need to reset your password, please click on the link below and "
"follow the instructions."
msgstr ""

#: canaille/templates/mail/firstlogin.txt:5
#: canaille/templates/mail/reset.html:38
msgid "Reset password"
msgstr ""

#: canaille/templates/mail/invitation.html:19
#: canaille/templates/mail/invitation.txt:1
msgid "Account creation"
msgstr ""

#: canaille/templates/mail/invitation.html:27
#, python-format
msgid ""
"You have been invited to create an account at %(site_name)s. To proceed "
"you can click on the \"Create my account\" button below and follow the "
"instructions."
msgstr ""

#: canaille/templates/mail/invitation.html:38
msgid "Create my account"
msgstr ""

#: canaille/templates/mail/invitation.txt:3
#, python-format
msgid ""
"You have been invited to create an account at %(site_name)s. To proceed "
"you can click on the link below and follow the instructions."
msgstr ""

#: canaille/templates/mail/reset.html:27
#, python-format
msgid ""
"Someone, probably you, asked for a password reinitialization link at "
"%(site_name)s. If you did not ask for this email, please ignore it. If "
"you need to reset your password, please click on the \"Reset password\" "
"button below and follow the instructions."
msgstr ""

#: canaille/templates/mail/reset.txt:3
#, python-format
msgid ""
"In order to finalize your account configuration at %(site_name)s, we need"
" to setup your password. Please click on the link below and follow the "
"instructions."
msgstr ""

#: canaille/templates/mail/test.html:27 canaille/templates/mail/test.txt:3
#, python-format
msgid ""
"This email is a test to check that you receive communications from "
"%(site_name)s."
msgstr ""

#: canaille/templates/oidc/admin/authorization_view.html:5
#: canaille/templates/oidc/admin/authorization_view.html:36
msgid "View an authorization"
msgstr ""

#: canaille/templates/oidc/admin/client_add.html:39
msgid "Confirm"
msgstr ""

#: canaille/templates/oidc/admin/client_edit.html:5
#: canaille/templates/oidc/admin/client_edit.html:54
msgid "Edit a client"
msgstr ""

#: canaille/templates/oidc/admin/client_edit.html:41
msgid "Client deletion"
msgstr ""

#: canaille/templates/oidc/admin/client_edit.html:44
msgid ""
"Are you sure you want to delete this client? This action is unrevokable "
"and all the data about this client will be removed."
msgstr ""

#: canaille/templates/oidc/admin/client_edit.html:60
msgid "ID"
msgstr ""

#: canaille/templates/oidc/admin/client_edit.html:69
msgid "Secret"
msgstr ""

#: canaille/templates/oidc/admin/client_edit.html:78
msgid "Issued at"
msgstr ""

#: canaille/templates/oidc/admin/client_edit.html:95
msgid "Delete the client"
msgstr ""

#: canaille/templates/oidc/admin/client_edit.html:98
msgid "Edit"
msgstr ""

#: canaille/templates/oidc/admin/token_view.html:5
#: canaille/templates/oidc/admin/token_view.html:57
msgid "Token details"
msgstr ""

#: canaille/templates/oidc/admin/token_view.html:42
msgid "Token deletion"
msgstr ""

#: canaille/templates/oidc/admin/token_view.html:46
msgid "Are you sure you want to revoke this token? This action is unrevokable."
msgstr ""

#: canaille/templates/oidc/admin/token_view.html:63
#: canaille/templates/partial/oidc/admin/authorization_list.html:6
#: canaille/templates/partial/oidc/admin/token_list.html:6
msgid "Client"
msgstr ""

#: canaille/templates/oidc/admin/token_view.html:71
#: canaille/templates/partial/oidc/admin/authorization_list.html:7
#: canaille/templates/partial/oidc/admin/token_list.html:7
msgid "Subject"
msgstr ""

#: canaille/templates/oidc/admin/token_view.html:89
msgid "Audience"
msgstr ""

#: canaille/templates/oidc/admin/token_view.html:103
msgid "Issue date"
msgstr ""

#: canaille/templates/oidc/admin/token_view.html:107
msgid "Expiration date"
msgstr ""

#: canaille/templates/oidc/admin/token_view.html:111
msgid "Revokation date"
msgstr ""

#: canaille/templates/oidc/admin/token_view.html:117
msgid "Revoke token"
msgstr ""

#: canaille/templates/oidc/admin/token_view.html:120
msgid "This token has not been revoked"
msgstr ""

#: canaille/templates/oidc/admin/token_view.html:126
msgid "Token type"
msgstr ""

#: canaille/templates/oidc/admin/token_view.html:130
msgid "Access token"
msgstr ""

#: canaille/templates/oidc/admin/token_view.html:141
msgid "Refresh token"
msgstr ""

#: canaille/templates/oidc/user/authorize.html:10
#, python-format
msgid "The application %(name)s is requesting access to:"
msgstr ""

#: canaille/templates/oidc/user/authorize.html:33
#, python-format
msgid "You are logged in as %(name)s"
msgstr ""

#: canaille/templates/oidc/user/authorize.html:40
msgid "Deny"
msgstr ""

#: canaille/templates/oidc/user/authorize.html:43
msgid "Switch user"
msgstr ""

#: canaille/templates/oidc/user/authorize.html:46
msgid "Accept"
msgstr ""

#: canaille/templates/oidc/user/consent_list.html:4
#: canaille/templates/oidc/user/consent_list.html:15
#: canaille/templates/oidc/user/consent_list.html:30
#: canaille/templates/oidc/user/preconsent_list.html:4
#: canaille/templates/oidc/user/preconsent_list.html:15
msgid "My consents"
msgstr ""

#: canaille/templates/oidc/user/consent_list.html:20
#: canaille/templates/oidc/user/preconsent_list.html:20
#: canaille/templates/oidc/user/preconsent_list.html:31
msgid "Pre-authorized applications"
msgstr ""

#: canaille/templates/oidc/user/consent_list.html:33
msgid "Consult and revoke the authorization you gave to websites."
msgstr ""

#: canaille/templates/oidc/user/consent_list.html:51
msgid "From:"
msgstr ""

#: canaille/templates/oidc/user/consent_list.html:54
msgid "Revoked:"
msgstr ""

#: canaille/templates/oidc/user/consent_list.html:59
msgid "Had access to:"
msgstr ""

#: canaille/templates/oidc/user/consent_list.html:61
#: canaille/templates/oidc/user/preconsent_list.html:51
msgid "Has access to:"
msgstr ""

#: canaille/templates/oidc/user/consent_list.html:84
#: canaille/templates/oidc/user/preconsent_list.html:73
msgid "Policy"
msgstr ""

#: canaille/templates/oidc/user/consent_list.html:92
#: canaille/templates/oidc/user/preconsent_list.html:81
msgid "Terms of service"
msgstr ""

#: canaille/templates/oidc/user/consent_list.html:99
msgid "Restore access"
msgstr ""

#: canaille/templates/oidc/user/consent_list.html:104
#: canaille/templates/oidc/user/preconsent_list.html:87
msgid "Revoke access"
msgstr ""

#: canaille/templates/oidc/user/consent_list.html:114
msgid "You did not authorize applications yet."
msgstr ""

#: canaille/templates/oidc/user/logout.html:9
#: canaille/themes/default/base.html:72
msgid "Log out"
msgstr ""

#: canaille/templates/oidc/user/logout.html:10
msgid "Do you want to log out?"
msgstr ""

#: canaille/templates/oidc/user/logout.html:14
#, python-format
msgid "You are currently logged in as %(username)s."
msgstr ""

#: canaille/templates/oidc/user/logout.html:16
#, python-format
msgid "The application %(client_name)s wants to disconnect your account."
msgstr ""

#: canaille/templates/oidc/user/logout.html:27
msgid "Stay logged"
msgstr ""

#: canaille/templates/oidc/user/logout.html:30
msgid "Logout"
msgstr ""

#: canaille/templates/oidc/user/preconsent_list.html:34
msgid "Those applications automatically have authorizations to access you data."
msgstr ""

#: canaille/templates/partial/groups.html:8
msgid "Number of members"
msgstr ""

#: canaille/templates/partial/groups.html:31
#: canaille/templates/partial/oidc/admin/authorization_list.html:31
#: canaille/templates/partial/oidc/admin/client_list.html:35
#: canaille/templates/partial/oidc/admin/token_list.html:39
#: canaille/templates/partial/users.html:63
msgid "No item matches your request"
msgstr ""

#: canaille/templates/partial/groups.html:33
#: canaille/templates/partial/oidc/admin/authorization_list.html:33
#: canaille/templates/partial/oidc/admin/client_list.html:37
#: canaille/templates/partial/oidc/admin/token_list.html:41
#: canaille/templates/partial/users.html:65
msgid "Maybe try with different criterias?"
msgstr ""

#: canaille/templates/partial/groups.html:36
#: canaille/templates/partial/oidc/admin/authorization_list.html:36
#: canaille/templates/partial/oidc/admin/client_list.html:40
#: canaille/templates/partial/oidc/admin/token_list.html:44
#: canaille/templates/partial/users.html:68
msgid "There is nothing here"
msgstr ""

#: canaille/templates/partial/oidc/admin/authorization_list.html:5
msgid "Code"
msgstr ""

#: canaille/templates/partial/oidc/admin/authorization_list.html:8
#: canaille/templates/partial/oidc/admin/client_list.html:8
#: canaille/templates/partial/oidc/admin/token_list.html:8
msgid "Created"
msgstr ""

#: canaille/templates/partial/oidc/admin/client_list.html:7
msgid "URL"
msgstr ""

#: canaille/templates/partial/oidc/admin/token_list.html:5
msgid "Token"
msgstr ""

#: canaille/themes/default/base.html:10
msgid "Authorization interface"
msgstr ""

#: canaille/themes/default/base.html:40
msgid "Profile"
msgstr ""

#: canaille/themes/default/base.html:47
msgid "Consents"
msgstr ""

#: canaille/themes/default/base.html:67
msgid "Admin"
msgstr ""
